import React from "react";
import {
  StyleSheet,
  TextInput,
  Text,
  TouchableOpacity,
  Image,
  ScrollView,
} from "react-native";
import { useForm, Controller, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { isEmpty } from "lodash";
import * as yup from "yup";

const schema = yup.object().shape({
  name: yup
    .string()
    .min(3, "Name must be between 3 and 20 characters")
    .max(20, "Name must be between 3 and 20 characters"),
  email: yup.string().email("Invalid email").required("Email is required"),
});

// resolvers tell react hook form how to validate the data
const LoginForm = () => {
  const {
    control,
    handleSubmit,
    formState: { errors },
    reset,
    watch,
  } = useForm({
    // validationSchema: schema,
    resolver: yupResolver(schema),
    defaultValues: {
      name: "",
      email: "",
    },
    // reValidateMode: "onSubmit", // occurs only on submit
  });

  // const nameWatch = useWatch({
  //   control,
  //   name: "name",
  //   defaultValues: "default name",
  // });
  // console.log(nameWatch);
  // const nameWatch = watch("name");
  // console.log(nameWatch);

  // console.log(control);

  function submit(data) {
    console.log(data);
    reset();
  }

  console.log(errors);

  return (
    <ScrollView contentContainerStyle={styles.container}>
      <Image style={styles.image} source={require("./assets/logo.png")} />
      <Text style={styles.title}>React Hook Form</Text>
      <Controller
        name="name" // references to the default name
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            placeholder="Name"
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            // onChangeText={(value) => onChange(value)}
            value={value}
          />
        )}
      />
      <Text style={styles.error}>{errors.name?.message}</Text>

      <Controller
        name="email"
        control={control}
        render={({ field: { onChange, onBlur, value } }) => (
          <TextInput
            placeholder="Email"
            style={styles.input}
            onBlur={onBlur}
            onChangeText={onChange}
            // onChangeText={(value) => onChange(value)}
            value={value}
          />
        )}
      />
      <Text style={styles.error}>{errors.email?.message}</Text>
      <TouchableOpacity
        disabled={isEmpty(errors) ? false : true} // checking if an object is empty is a pain in js
        onPress={handleSubmit(submit)}
      >
        <Text style={styles.button}>Submit</Text>
        {/* <Text style={{ ...styles.button, backgroundColor: isEmpty(errors) ? "#c01c00" : "#999" }}>
          Submit
        </Text> */}
      </TouchableOpacity>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#282828",
    alignItems: "center",
    justifyContent: "center",
  },
  title: {
    fontSize: 36,
    marginBottom: 30,
    marginTop: 16,
    color: "white",
  },
  error: {
    fontSize: 16,
    color: "red",
    marginTop: 16,
    marginBottom: 16,
    marginLeft: 36,
    marginRight: 36,
  },
  input: {
    fontSize: 18,
    borderWidth: 1,
    padding: 12,
    width: "80%",
    borderRadius: 10,
    backgroundColor: "white",
  },
  image: {
    width: 120,
    height: 120,
    borderColor: "orange",
    borderWidth: 2,
    borderRadius: 100,
  },
  button: {
    fontSize: 20,
    color: "white",
    width: 120,
    marginTop: 8,
    borderRadius: 10,
    backgroundColor: "#c01c00",
    padding: 8,
    textAlign: "center",
  },
});

export default LoginForm;
